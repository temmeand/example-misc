import ConfigParser

config = ConfigParser.RawConfigParser()

sect = 'Section1'
config.add_section(sect)
config.set(sect, 'an_int', '15')
config.set(sect, 'a_bool', 'true')
config.set(sect, 'a_float', '3.141')
config.set(sect, 'baz', 'fun')
config.set(sect, 'bar', 'Python')
config.set(sect, 'foo', '%(bar)s is %(baz)s!')
config.add_section('Se2')
config.set('Se2', 'start', '15')
config.set('Se2', 'stop', '6e9')

with open('examples.cfg', 'wb') as configfile:
    config.write(configfile)
    
cfg = ConfigParser.RawConfigParser()
cfg.read('examples.cfg')
a_float = cfg.getfloat(sect, 'a_float')
an_int = cfg.getboolean(sect, 'a_bool')
print a_float + an_int

print cfg.get(sect, 'foo')

c = ConfigParser.ConfigParser()
c.read('examples.cfg')
print c.get(sect, 'foo', 0)
print c.get(sect, 'foo', 1)

print c.get(sect, 'foo', 0, {'bar': 'Documentation',
                             'baz': 'evil'})

naCfg = ConfigParser.SafeConfigParser({'start':'13', 'dog':'colter'},allow_no_value=True)

sect = 'vnaConfig'
naCfg.add_section(sect)
# GPIB Address - Value will be appended to GPIB::
naCfg.set(sect, 'gpibAddres', '16')
naCfg.set(sect, 'gpibTimeout', '120')
#Start frequency in MHz - minimum 0.03 (30kHz)
naCfg.set(sect, 'start', '0.03')
#Stop frequency in MHz - maximum 6000 (6GHz)
naCfg.set(sect, 'stop', '6000')
#Number of Points - Possible values: 
# 3, 11, 21, 26, 51, 101, 201, 401, 801, 1601
naCfg.set(sect, 'numPts', '201')
#Avg Factor - set to 0 to turn off averaging
naCfg.set(sect, 'avgFact', '8')
#Power range -85 to +10 dBm
naCfg.set(sect, 'power', '10')
#IFBW possible values (Hz): 10, 30, 100, 300, 1000, 3000, 3700
naCfg.set(sect, 'ifbw', '3000')
#Leave cal file lines blank if you want to take cal measurements
naCfg.set(sect, 'openCalFile')
naCfg.set(sect, 'plateCalFile')

with open('configTemplate.txt', 'w') as cfgFile:
    naCfg.write(cfgFile)

cfg = ConfigParser.SafeConfigParser(allow_no_value=True)
cfg.read('configTemplate.txt')
print('-----------')
for sect in cfg.sections():
    print cfg.items(sect)


