Miscellaneous Examples
======================

This repository contains miscellaneous examples. Most have been written or put
together by me. Please feel free to re-use with attribution. 

Repo Availability/Locations
---------------------------

This repository is mirrored on https://bitbucket.org/temmeand/ and
https://gitlab.msu.edu/u/temmeand.

      
Errors/Problems
---------------

Please contact me or submit a pull request if you find an error. Thank you.
